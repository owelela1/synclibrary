package com.pixel.library.sync

import android.app.Activity
import io.reactivex.Observable
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class SyncTest {

    companion object {

        // region constants ------------------------------------------------------------------------

        private const val TIMES_SYNC = 30
        private const val MIN_CHARGE = 20
        private const val IS_METERED_ACCEPTED = false

        // endregion constants ---------------------------------------------------------------------

        // region helper fields --------------------------------------------------------------------

        @Mock
        var mMockActivity: Activity? = null

        @Mock
        var mMockSyncData: Observable<Any>? = null

        // endregion helper fields -----------------------------------------------------------------

        private var SUT: Sync? = null
    }


    @Before
    fun setUp() {
        SUT = Sync()
    }

    @Test
    fun sync_setOnSyncListener() {

        //Arrange
        var result: Observable<Any>? = null

        //Act
        SUT!!.setOnSyncListener(object : OnSyncListener {
            override fun onSync(syncData: Observable<Any>?) {
                result = syncData
            }

        })

        //Assert
        assertThat(result, nullValue())
    }

    @Test
    fun sync_success_doSync() {

        //Arrange
        var result: Observable<Any>? = null

        //Act
        SUT!!.setOnSyncListener(object : OnSyncListener {
            override fun onSync(syncData: Observable<Any>?) {
                result = syncData
            }

        })

//        SUT!!.doSync(mMockActivity, mMockSyncData, TIMES_SYNC, MIN_CHARGE, IS_METERED_ACCEPTED)

        //Assert
        assertThat(result, nullValue())
    }

    @Test
    fun sync_stopSync() {
        //Act
        SUT!!.stopSync()
    }

    @After
    fun clearMocks() {
        Mockito.framework().clearInlineMocks()
    }
}