package com.pixel.library.sync

import io.reactivex.Observable

interface OnSyncListener {
    fun onSync(syncData: Observable<Any>?)
}
