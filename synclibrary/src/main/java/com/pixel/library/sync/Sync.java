package com.pixel.library.sync;

import android.app.Activity;
import android.os.CountDownTimer;
import android.util.Log;

import com.pixel.library.util.battery.BatteryUtils;
import com.pixel.library.util.common.CommonUtils;

import io.reactivex.Observable;

public class Sync {

    private OnSyncListener mOnSyncListener;

    private CountDownTimer mCountTimer = null;

    private Activity mActivity = null;
    private Observable<Object> mSyncData = null;
    private int mTimesToSyncHr = 0;
    private int mMinBatteryLevel = 0;
    private boolean mIsMeteredAccepted = false;

    private boolean isSync = true;

    public void setOnSyncListener(OnSyncListener listener) {
        mOnSyncListener = listener;
    }

    public void doSync(
            Activity activity,
            Observable<Object> syncData,
            int timesToSyncHr,
            int minBatteryLevel,
            boolean isMeteredAccepted) {

        mActivity = activity;
        mSyncData = syncData;
        mTimesToSyncHr = timesToSyncHr;
        mMinBatteryLevel = minBatteryLevel;
        mIsMeteredAccepted = isMeteredAccepted;

        if (timesToSyncHr > 0) {

            int counterTimeInterval = timesToSyncHr * 3600000;
            Log.e("Sync ", "Sync application listener!");

            if (CommonUtils.INSTANCE.isNetworkConnectedMetered(activity, isMeteredAccepted)) {
                if (isSync) {
                    if (BatteryUtils.isBatteryStatusGood(activity, minBatteryLevel)) {
                        isSync = false;
                        mOnSyncListener.onSync(null);
                        startTimer(counterTimeInterval);

                        Log.e("Sync ", "Syncing!");
                    }
                }
            }
        }

    }

    private void startTimer(int time) {
        mCountTimer = new CountDownTimer(time, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.e("Sync Time Remaining: ", String.valueOf(millisUntilFinished));
            }

            public void onFinish() {
                isSync = true;
                doSync(mActivity, mSyncData, mTimesToSyncHr, mMinBatteryLevel, mIsMeteredAccepted);
            }
        };
        mCountTimer.start();
    }


    //cancel timer
    private void cancelTimer() {
        if (mCountTimer != null)
            mCountTimer.cancel();
    }

    public void stopSync() {
        cancelTimer();
    }

}
